using PhageFields
using NLopt
include("log_approx.jl")
include("utilities.jl")

#variants that disappear at a certain round and than re-appear
#again MUST be filtered out to avoid singularities

"""
    Wrapper for parameters that define the model for variants and amplification phase
"""
mutable struct Model{M}
    epistasis::M
    x::Vector{Float64}
    λ::Vector{Float64}
    A::Int
    L::Int
    T::Int
    V::Int
end

function Model(epi_type::Type, data)
    epistasis = epi_type{data.A, data.L}()
    x = zeros(Float64, length(epistasis))
    λ = zeros(Float64, data.T-1)
    return Model(epistasis, x, λ, data.A, data.L, data.T, data.V)
end

"""
    Computes the energy of a variant
"""
function Energy(model::Model, variant)
    return energy(model.epistasis, model.x, variant)
end

"""
    Computes the binding ener
"""
function ps(model::Model, variant)
    return 1/(1+exp(Energy(model, variant)))
end

"""
    Computes the likelihood for the array of variants in variants
"""
function Likelihood(model::Model, variants, counts)
    L=0.0
    for t in 1:length(model.λ)
        for s in 1:length(variants)
            if counts[s,1,t]==0
                continue
            else
                hs = Energy(model, variants[s])
                if counts[s,1,t+1]==0
                    if exp(-hs)==Inf
                        L += counts[s,1,t]*hs
                    else
                        L -= counts[s,1,t]*log(1+exp(-hs))
                    end
                else
                    if exp(-hs)==Inf
                        L += log_binomial(counts[s,1,t], counts[s,1,t+1]/model.λ[t])+counts[s,1,t]*hs-(counts[s,1,t+1]/model.λ[t])*hs
                    else
                        L += log_binomial(counts[s,1,t], counts[s,1,t+1]/model.λ[t])
                        -counts[s,1,t]*log(1+exp(-hs))-(counts[s,1,t+1]/model.λ[t])*hs
                    end
                end
            end
        end
        L-=length(variants)*log(model.λ[t])
    end
    return L
end

"""
    Computes the gradient of the likelihood with respect to λ
"""
function Likelihood_gradient_lambda(model::Model, variants, counts)
    δ=zeros(length(model.λ))
    for t in 1:length(model.λ)
        for s in 1:length(variants)
            if counts[s,1,t+1]==0
                continue
            else
                hs=Energy(model, variants[s])
                δ[t] += (counts[s,1,t+1]/model.λ[t]^2)*(log(counts[s,1,t+1]/model.λ[t])-log(counts[s,1,t]-counts[s,1,t+1]/model.λ[t])+hs)
            end
        end
        δ[t] -= length(variants)/model.λ[t]
    end
    return δ
end
