function not_disappearing(data)
    idx=Array{Int, 1}(undef,0)
    for s in 1:data.S
        flag = true
        for t in 1:data.T-1
            if data.counts[s,1,t]==0 && data.counts[s,1,t+1]>0
                flag = false
                break
            end
        end

        if flag
            push!(idx,s)
        end
    end
    return idx
end
