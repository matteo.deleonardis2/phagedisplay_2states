function log_binomial(n::Float64,k::Float64)
    if n==0
        return 0
    elseif k==0
        return 0
    elseif n==k
        return 0
    else
        return n*log(n)-k*log(k)-(n-k)*log(n-k)
    end
end
