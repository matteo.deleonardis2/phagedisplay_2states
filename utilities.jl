function not_disappearing(data)
    idx=[]
    for s in 1:data.S
        flag = true
        for t in 1:data.T-1
            if data.counts[s,1,t]==0 && data.counts[s,1,t+1]>0
                flag = false
                break
            end
        end

        if flag
            push!(idx,s)
        end
    end
    return idx
end

function filter_counts(data, rounds::Vector{Int}, counts_threshold::Int)
    @assert maximum(rounds) <= data.T
    @assert length(rounds) <= data.T

    data_filtered = deepcopy(data)
    for t in rounds
        data_filtered = subdata(data, findall(data.counts[:,1,t] .>= counts_threshold))
    end
    return data_filtered
end

function traintest_split(data, folds::Vector, k::Int)
    ind_test = ind_folds[k]
    ind_train = vcat(ind_folds[1:end .!= k]...)
    data_test = subdata(data, ind_test)
    data_train = subdata(data, ind_train)
    return (data_train, ind_train), (data_test, ind_test)
end

function starting_point!(model::Model, data; μ::Float64 = -5.0, δλ = 10.0)
    randn!(model.x)
    model.x ./ model.L
    for t in 1:model.T-1
        model.λ[t] = λmin(data.counts, t) + δλ
    end
end

function set_prior(variance::Float64, model::Model)
    var = fill(variance, fields_length(model.epistasis))
    numJ = (model.A)^2 * model.L*(model.L-1)÷2
    numh = model.A*model.L
    for l in 1:numJ
        var[l] /= numJ
    end
    for l in 1:numh
        var[numJ+l] /= numh
    end

    return GaussianPrior(var)
end

function set_optimizer(algorithm::Symbol, model::Model, data; ftol_rel = 0.0, ftol_abs = 0.0, xtol_rel = 0.0, xtol_abs = 0.0, Δλ = 1e-10)
    opt = Opt(algorithm, Model_length(model))
    opt.ftol_rel = ftol_rel
    opt.ftol_abs = ftol_abs
    opt.xtol_rel = xtol_rel
    opt.xtol_abs = xtol_abs
    lb = opt.lower_bounds
    lb[end-model.T+2:end] .= [λmin(data.counts, t) + Δλ for t in 1:model.T-1]
    opt.lower_bounds = lb
    return opt
end

function add_pseudocounts(data, pc)
    data_new = deepcopy(data)
    data_new.counts .+= pc
    return data_new
end

function log_range(start, stop, points)
    r = LinRange(log(start), log(stop), points)
    r = exp.(r)
    return r
end

function spearman_selectivity_filters_curves( data, predictions::Vector{Vector{Float64}};
    plot::Bool=true,
    selectivity=data.θ,
    filter_feature=-data.Δ,
    thresh=compute_thresh(filter_feature),
    labels=[string(i) for i=1:length(predictions)])

    Sf=length(filter_feature)
    n_predictions=size(predictions)[1]

    v_thr_cor=zeros(length(thresh),n_predictions+1)
    for t=1:length(thresh)

            filter_idx = [s for s = 1 : Sf if filter_feature[s] ≥ thresh[t]];

            if !isempty(filter_idx)
                v_thr_cor[t,1] = length(filter_idx)/Sf;
                for k=1:n_predictions
                    v_thr_cor[t,2] = corspearman(predictions[k][filter_idx],selectivity[filter_idx])
                end
            end
    end

    if plot

        for k=1:n_predictions
            semilogx(v_thr_cor[:,1],v_thr_cor[:,k+1],label=labels[k])
        end
        ylabel("Spearman correlation")
        xlabel("fraction of sequences")
        legend()
        title("correlation with mean selectivity")
    else
        return v_thr_cor
    end

end

function avg_selectivity_curve(θcurves_train)
    avg_corr_train = Array{Float64, 2}(undef,size(θcurves_train[1]))

    for j in 1:size(avg_corr_train)[1]
        avg_corr_train[j,1] = θcurves_train[1][j,1]
        avg_corr_train[j,2] = mean([θcurves_train[k][j,2] for k in 1:length(θcurves_train)])
    end

    std_corr_train = Array{Float64, 1}(undef, size(avg_corr_train)[1])

    for j in 1:length(std_corr_train)
        std_corr_train[j] = std([θcurves_train[k][j,2] for k in 1:length(θcurves_train)])
    end
    return avg_corr_train, std_corr_train ./ sqrt(length(θcurves_train))
end


function create_folds(S::Int, n_folds::Int)
    samples_per_fold = S ÷ n_folds
    fold_samples = fill(samples_per_fold, n_folds-1) #temporary
    push!(fold_samples, S - samples_per_fold*(n_folds-1))
    idx_folds = randsplit(S, fold_samples...)
end
