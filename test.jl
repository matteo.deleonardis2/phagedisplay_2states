ENV["PHAGEDATAPATH"]="/home/matteo/poliTo/Tesi/Codes/2stati/Data"
using PhageData
data = boyer2016pnas(:f3pvp, :aa; gaps=false)
data=subdata(data,not_disappearing(data))
model = Model(EpistasisMu, data)
model.λ=[20.0,20.0]




epistasis=EpistasisMu{20,4}()
model=Model(epistasis)
prior=GaussianPrior(100*ones(length(model.model)))
fit_parameters(model,prior,data)


Nmax=maximum(data.counts)
Ms=M(data, scaling=Nmax)
function lambda_like(x::Array{Float64,1},grad::Array{Float64,1})
    λ=x[end-data.T+2:end]
    model.x=x[1:end-data.T+1]
    L=Likelihood(model,λ,data)
    ms=m(λ,data,scaling=Nmax)
    Δ=Nmax.*gradient_likelihood_ps(model,ms,Ms,data)
    Λ=gradient_likelihood_lambda(model,λ,data)
    if length(grad)>0
        grad.=vcat(Δ,Λ)
        prior(grad,x)
    end
    #verbose && println("likelihood=$L    ","grad=",extrema(grad))
    return L
end
λmin=λ_min(data)
lb=vcat(fill(-Inf,length(model.model)),λmin.+1e-12)
optN = Opt(:LD_LBFGS,length(model.model)+data.T-1)
optN.lower_bounds=lb
optN.xtol_rel = 1e-5
optN.ftol_rel = 1e-5
optN.max_objective = lambda_like
rand_start=rand(length(model.model))
point=vcat(rand_start,1.0.+λmin)
maxf,maxx,ret= optimize(optN, vcat(rand_start,1.0.+λmin))
λbest=maxx[end-data.T+1:end]
pars=maxx[1:end-data.T]
model.x=pars
return λbest,pars,ret




λ=x[end-data.T+2:end]
model.x=x[1:end-data.T+1]
L=Likelihood(model,λ,data)
ms=m(λ,data,scaling=Nmax)
Δ=Nmax.*gradient_likelihood_ps(model,ms,Ms,data)
Λ=gradient_likelihood_lambda(model,λ,data)
if length(grad)>0
    grad.=vcat(Δ,Λ)
    prior(grad,x)
end
verbose &&
println("likelihood=$L    ","grad=",extrema(grad))
return L
