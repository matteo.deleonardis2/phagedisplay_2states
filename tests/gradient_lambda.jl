ENV["PHAGEDATAPATH"] = "/home/matteo/poliTo/Tesi/Data/"
using PhageData, Zygote, Random, PhageFields
include("/home/matteo/CODE/phagedisplay_2states/2states.jl")

data = boyer2016pnas(:f3pvp,:aa; gaps=false);
data = subdata(data, findall(data.Δ.<0.5))
data = subdata(data, not_disappearing(data))

epistasis = EpistasisMu{data.A, data.L}()
λm = [λmin(data.counts, 1), λmin(data.counts,2)]
λ = [80.1, 31.7]
for l in 1:data.T-1
    @assert λ[l] > λm[l]
end

σ = 4.0
x = σ .* randn(fields_length(epistasis))


model = Model(epistasis,data.T,x,λ)

function f(λ)
    mod = Model(epistasis,3,x,λ)
    return Likelihood_lambda(mod,data)
end

grad_zygote = f'(λ)

grad = gradient_Likelihood_lambda(model, data)

max_err = extrema(abs.(grad_zygote - grad))
println("minimum and maximum discrepancy of components with respect to the ones computed by zygote (in absolute value): \n", max_err)
