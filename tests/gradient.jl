ENV["PHAGEDATAPATH"] = "/home/matteo/poliTo/Tesi/Data/"
using PhageData, Zygote, Random, PhageFields
include("/home/deleonardis/CODE/phagedisplay_2states/2states.jl")

data = boyer2016pnas(:f3pvp,:aa; gaps=false);
#data = subdata(data, findall(data.Δ.<0.5))
data = subdata(data, not_disappearing(data))
data=subdata(data, findall(data.counts[:,1,1] .> 10))

epistasis = EpistasisMu{data.A, data.L}()
λm = [λmin(data.counts, 1), λmin(data.counts,2)]
λ = λm .+ 10
for l in 1:data.T-1
    @assert λ[l] > λm[l]
end

σ = 100.0
x = σ .* randn(fields_length(epistasis))
x[end] = 5.0


model = Model(epistasis,data.T,x,λ)

function f(vec)
    mod = Model(epistasis,3,vec[1:end-2],vec[end-1:end])
    return Likelihood(mod,data)
end

v = vcat(x,λ)
grad_zygote = f'(v)

grad = gradient_Likelihood(model, data)

max_err = extrema(abs.(grad_zygote - grad))
println("minimum and maximum discrepancy of components with respect to the ones computed by zygote (in absolute value): \n", max_err)
