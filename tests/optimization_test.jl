ENV["PHAGEDATAPATH"] = "/home/matteo/poliTo/Tesi/Data/"
using PhageData, Zygote, Random, PhageFields
include("/home/matteo/CODE/phagedisplay_2states/2states.jl")

data = boyer2016pnas(:f3pvp,:aa; gaps=false);
data = subdata(data, findall(data.Δ.<0.5))
data = subdata(data, not_disappearing(data))

epistasis = EpistasisMu{data.A, data.L}()
model = Model(epistasis, data.T, randn(fields_length(epistasis)), [20.0, 20.0])
prior = GaussianPrior(fill(10.0, fields_length(model.epistasis)))

learn!(model, prior, data; ftol = 1e-4)
