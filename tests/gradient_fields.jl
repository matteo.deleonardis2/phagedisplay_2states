ENV["PHAGEDATAPATH"] = "/home/matteo/poliTo/Tesi/Data/"
using PhageData, Zygote, Random, PhageFields
include("/home/matteo/CODE/phagedisplay_2states/2states.jl")

data = boyer2016pnas(:f3pvp,:aa; gaps=false);
data = subdata(data, findall(data.Δ.<0.5))

epistasis = EpistasisMu{data.A, data.L}()
λ = [32.0,55.0]

σ = 4.0
x = σ .* randn(fields_length(epistasis))


model = Model(epistasis,data.T,x,λ)

function f(x)
    mod = Model(epistasis,3,x,λ)
    return Likelihood_fields(mod,data)
end

grad_zygote = f'(x)

grad = gradient_Likelihood_fields(model, data)

max_err = extrema(abs.(grad_zygote - grad))
println("minimum and maximum discrepancy of components with respect to the ones computed by zygote (in absolute value): \n", max_err)
