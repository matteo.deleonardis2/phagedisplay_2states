#including packages dependencies
include("packages.jl")

#includes
include("import_methods.jl")
include("log_approx.jl")
include("2states.jl")
include("utilities.jl")
include("inference.jl")
