function inference(model::Model, data)
    energies = [Energy(model, s) for s in data.variants]
    p = [pb(model, s) for s in data.variants]
    return (energies, p)
end

function plot_distribution(energies::Vector{Float64}, ps::Vector{Float64})
    ind = sortperm(energies, rev=true)
    plot(energies[ind], ps[ind])
    xlabel("Es")
    ylabel("ps")
end

function deterministic_binding(pb::Vector{Float64}, λ::Float64, data, t::Int; count_thr = 20)
    ind = findall(data.counts[:,1,t] .> count_thr);
    average = pb[ind] .* data.counts[ind,1,t]
    variance = pb[ind] .* (1.0 .- pb[ind]) .* data.counts[ind,1,t]
    line = LinRange(0.0, max(maximum(data.counts[ind,1,t+1] ./ λ), maximum(average)),10)
    errorbar(data.counts[ind,1,t+1] ./ λ, average, sqrt.(variance), ls="None", marker="s")
    corr11 = cor(data.counts[ind,1,t+1] ./ λ, average)

    plot(line,line, linewidth=1, color="red")
    title("(t=$t)   corr= $corr11")
    ylabel("N(s)ps")
    xlabel("Ns(t+1)/λ(t)")
end

function log_deterministic_binding(pb::Vector{Float64}, λ::Float64, data, t::Int; count_thr = 20)
    ind = findall(data.counts[:,1,t] .> count_thr);
    average = pb[ind] .* data.counts[ind,1,t]
    variance = pb[ind] .* (1.0 .- pb[ind]) .* data.counts[ind,1,t]
    line = LinRange(0.0, max(maximum(data.counts[ind,1,t+1] ./ λ), maximum(average)),10)
    xscale(:log)
    yscale(:log)
    errorbar(data.counts[ind,1,t+1] ./ λ, average, sqrt.(variance), ls="None", marker="s")
    corr11 = cor(data.counts[ind,1,t+1] ./ λ, average)

    plot(line,line, linewidth=1, color="red")
    title("(t=$t)   corr= $corr11")
    ylabel("N(s)ps")
    xlabel("Ns(t+1)/λ(t)")
end
