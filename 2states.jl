
struct Model{A,L}
	epistasis::EpistasisMu{A,L}
	x::Vector{Float64}
	λ::Vector{Float64}
	A::Int
	L::Int
	T::Int
end

function Model(epistasis::EpistasisMu{A,L}, T::Int, x = [], λ = []) where {A,L}
	if length(x) == 0
		x = zeros(fields_length(epistasis))
	end
	if length(λ) == 0
		λ = zeros(T-1)
	end
	return Model(epistasis, x, λ, A, L, T)
end

function Model_length(model::Model)
	return fields_length(model.epistasis) + model.T -1
end

"""
	Energy of a variant
"""
function Energy(model::Model, variant)
	return energy(model.epistasis, model.x, variant)
end

"""
	binding probability
"""
function pb(model::Model, variant)
	E = energy(model.epistasis, model.x, variant)
	return 1/(1+exp(E))
end

function ms(Ns::Vector{Float64}, λ) #Ns are counts of variant s throut the various rounds
	m = 0.0
	for t in 1:length(Ns)-1
		m += Ns[t+1]/λ[t]
	end
	return m
end

function Ms(Ns::Vector{Float64})
	M = 0.0
	for t in 1:length(Ns)-1
		M += Ns[t]
	end
	return M
end

function λmin(N::Array{Float64,3},t::Int)
	l = -Inf
	for s in 1:size(N)[1]-1
		if N[s,1,t] == 0
			continue
		end
		l = max(l, N[s,1,t+1]/N[s,1,t])
	end
	return l
end

function Likelihood_fields(model::Model, data)
	L = 0.0
	for s in 1:data.S
		Hs = Energy(model, data.variants[s])
		m = ms(data.counts[s,1,:], model.λ)
		M = Ms(data.counts[s,1,:])
		L += -M*log_oneplusexp(Hs) + (M-m)*Hs
	end
	return L
end

function gradient_Likelihood_fields(model::Model, data)
	grad = zeros(length(model.x))
	for s in 1:data.S
		p = pb(model, data.variants[s])
		m = ms(data.counts[s,1,:], model.λ)
		M = Ms(data.counts[s,1,:])
		g = m - p*M
		iter = fields_iterator(model.epistasis, data.variants[s])
		for (f,m) in iter
			grad[f] += g
		end
	end
	return grad
end

function Likelihood_lambda(model::Model, data)
	L = 0.0
	H = [Energy(model, data.variants[s]) for s in 1:data.S]
	for t in 1:data.T-1
		for s in 1:data.S
			L += log_binom(data.counts[s,1,t], data.counts[s,1,t+1]/model.λ[t]) - H[s]*data.counts[s,1,t+1]/model.λ[t]
		end
		L -= data.S*log(model.λ[t])
	end
	return L
end

function gradient_Likelihood_lambda(model::Model, data)
	grad = zeros(model.T-1)
	H = [Energy(model, data.variants[s]) for s in 1:data.S]
	for t in 1:model.T-1
		for s in 1:data.S
			grad[t] += ( xlogy(data.counts[s,1,t+1]/model.λ[t]^2, data.counts[s,1,t+1]/model.λ[t])
						- xlogy(data.counts[s,1,t+1]/model.λ[t]^2, data.counts[s,1,t]-data.counts[s,1,t+1]/model.λ[t])
						+ H[s]*data.counts[s,1,t+1]/model.λ[t]^2)
		end
		grad[t] -= data.S/model.λ[t]
	end
	return grad
end

function Likelihood(model::Model, data)
	L = 0.0
	H = [Energy(model, data.variants[s]) for s in 1:data.S]
	for t in 1:model.T-1
		for s in 1:data.S
			L += ( log_binom(data.counts[s,1,t], data.counts[s,1,t+1]/model.λ[t])
					- H[s]*data.counts[s,1,t+1]/model.λ[t] - log_oneplusexp(-H[s])*data.counts[s,1,t] )
		end
		L -= data.S*log(model.λ[t])
	end
	return L
end

function gradient_Likelihood(model::Model, data)
	gf = gradient_Likelihood_fields(model, data)
	gl = gradient_Likelihood_lambda(model, data)
	return vcat(gf, gl)
end

function λmle(model::Model, data, t::Int)
	a = 0.0
	b = 0.0
	for s in 1:data.S
		e = Energy(model, data.variants[s])
		a += (1 + exp(-e))*data.counts[s,1,t+1]
		b += exp(-e)*data.counts[s,1,t]
	end
	b += data.S
	return a/b
end

function gradient_λmle(model::Model, data, t::Int)
	grad = zeros(length(model.x))
	a = 0.0
	b = 0.0
	for s in 1:data.S
		e = Energy(model, data.variants[s])
		a += (1 + exp(-e))*data.counts[s,1,t+1]
		b += exp(-e)*data.counts[s,1,t]
	end
	b += data.S
	for s in 1:data.S
		e = Energy(model, data.variants[s])
		ft = exp(-e)*data.counts[s,1,t+1]
		et = exp(-e)*data.counts[s,1,t]
		iter = fields_iterator(model.epistasis, data.variants[s])
		for (i,m) in iter
			grad[i] += ft/b - (a/(b^2))*et
		end
	end
	return grad
end

#every property to compute should be a function of the model, prior and data
function learn!(model::Model, prior::GaussianPrior, opt::Opt, data; monitor = nothing)
	λm = [λmin(data.counts, t) for t in 1:model.T-1]
	for t in 1:model.T-1
		@assert model.λ[t] >= λm[t]
	end
	xinit = vcat(model.x, model.λ)
	logP = []
	properties = [[] for a in monitor]
	function f(x::Vector, grad::Vector, vals::Vector, prop::Vector)
		model.x .= x[1:end-model.T+1]
		model.λ .= x[end-model.T+2:end]
		if length(grad) > 0
			grad .= gradient_Likelihood(model, data)
			prior(grad[1:end-model.T+1], x[1:end-model.T+1])
		end
		l = Likelihood(model, data) + prior(x[1:end-model.T+1])
		push!(vals, l)
		if monitor != nothing
			for i in 1:length(monitor)
				push!(prop[i], monitor[i](model, prior, data)[:])
			end
		end
		return l
	end

	f1(x::Vector, grad::Vector) = f(x, grad, logP, properties)
	opt.max_objective = f1
	(maxf, xmax, ret) = optimize(opt, xinit)
	model.x .= xmax[1:end-model.T+1]
	model.λ .= xmax[end-model.T+2:end]
	return (model, ret, logP, properties)
end


#every property to compute should be a function of the model, prior and data
function learn_mle!(model::Model, prior::GaussianPrior, opt::Opt, data; monitor = [])
	xinit = model.x
	logP = []
	properties = [[] for a in monitor]
	function f(x::Vector, grad::Vector, vals::Vector, prop::Vector)
		model.x .= x
		model.λ .= [λmle(model, data, t) for t in 1:model.T-1]
		if length(grad) > 0
			grad .= gradient_Likelihood_fields(model, data)
			prior(grad, x)
		end
		l = Likelihood_fields(model, data) + prior(x)
		push!(vals, l)
		if length(monitor) > 0
			for i in 1:length(monitor)
				push!(prop[i], monitor[i](model, prior, data)[:])
			end
		end
		return l
	end

	f1(x::Vector, grad::Vector) = f(x, grad, logP, properties)
	opt.max_objective = f1
	(maxf, xmax, ret) = optimize(opt, xinit)
	model.x .= xmax
	model.λ .= [λmle(model, data, t) for t in 1:model.T-1]
	return (model, ret, logP, properties)
end
